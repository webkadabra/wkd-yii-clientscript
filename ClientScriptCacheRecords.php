<?php
/**
 * @author Sergii 'b3atb0x' <hello@webkadabra.com>
 */
class ClientScriptCacheRecords extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'ClientScript_cacheRecords':
	 * @var string $id
	 * @var string $script_type
	 * @var string $alias
	 * @var integer $version
	 * @var string $clientside_url
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ClientScript_cacheRecords';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('version', 'required'),
			array('version', 'numerical', 'integerOnly'=>true),
			array('id, clientside_url', 'length', 'max'=>255),
			array('script_type', 'length', 'max'=>3),
			array('alias', 'length', 'max'=>50),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'script_type' => 'Script Type',
			'alias' => 'Alias',
			'version' => 'Version',
			'clientside_url' => 'Clientside Url',
		);
	}
	
	//------------------------------------------------------------
	
	public function getCachedFilePath($id, $type)
	{
		$data = self::model()->find('id=? AND script_type=?',array($id, $type));
	}
}