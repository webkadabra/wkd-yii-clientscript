/**
 * Webkadabra Wkd Clientside Core
 * @depends jquery-core (>=1.3.3)
 * @provides wkd-core
 */
var wkd = function (jQuery) {
	var BootloadScripts;
	
    function n(z) {
        var w = wkd;
        if (z.indexOf(".") == -1) return typeof w.lib[z] == "undefined" ? null : w.lib[z];
        else jQuery.each(z.split("."), function (D, G) {
            w = typeof w[G] == "undefined" ? null : w[G]
        });
        return w
    }
	/**
	 * Dispatch message
	 * @param string Message Type
	 * @param
	 * @param
	 */
	function Msg(msgType, msgBody, z) {
		if (frmwrk.isActive("widgets.notifier")) ! z || typeof z.args == "undefined" ? wkd.widgets.notifier[msgType](msgBody, z) : wkd.widgets.notifier[msgType].apply(window, z.args);
		else
			alert(msgType.toUpperCase() + ":\n\n" + msgBody)
	}
    var d = {}, // Loaded widgets
        frmwrk = null;
		
    var Pub = {
            bu: "",
            isLoading: false,
            widgets: {},
            views: {},
            messages: {},
            lib: {
                isActive: function (z) {
                    return d[z] === true
                },
                call: function (z, w, D) {
                    var G = z;
                    return jQuery.isFunction(G) || jQuery.isFunction(G = n(G)) ? w === null && jQuery.isArray(D) ? G.apply(z, D) : G.call(z, w) : Function("a", "args", z + (w === null && jQuery.isArray(D) ? ".apply(window,args);" : ".call(window,a);"))(w, D)
                },
				
                /**
                 * Set AJAX handler as wkd.lib.dispatchResponse for it to call necessary libs
                 * @access public
                 */
                dispatchResponse: function (z) {
                    Pub.isLoading = false;
                    if (jQuery.isPlainObject(z)) {
                        var w = Pub.lib;
                        jQuery.each(z, function (D, G) {
                            typeof G.args == "undefined" ? w.call(D, G) : w.call(D, null, G.args)
                        })
                    }
                },
                message: function (z, w) {
                    Msg("message", z, w)
                },
                error: function (z, w) {
                    Msg("error", z, w)
                },
                success: function (z, w) {
                    Msg("success", z, w)
                },
                notify: function (z, w) {
                    Msg("notify", z, w)
                },
                confirm: function (z, w) {
                    Msg("confirm", z, w)
                },
				
                refresh: function () {
                    window.location.reload()
                },
                redirect: function (z) {
                    window.location = z
                },
                lazyLoadImages: function (z) {
                    var w = jQuery("img[original]"),
                        D;
                    D = setInterval(function () {
                        jQuery.each(w, function (G, F) {
                            F = jQuery(F);
                            if (F.is(":visible")) {
                                F.attr("src", F.attr("original"));
                                w.splice(G, 1)
                            }
                        });
                        w.length === 0 && clearInterval(D)
                    }, Math.max(z || 50, 10))
                },
                getHashUrlParams: function () {
                    var z = window.location.hash ? window.location.hash.substr(1) : false,
                        w = {};
                    z && jQuery.each(z.split("&"), function (D, G) {
                        D = G.split("=", 2);
                        w[D[0]] = D[1] ? decodeURIComponent(D[1]) : null
                    });
                    return w
                },
                setHashUrlParam: function (z, w) {
                    var D = w ? {} : frmwrk.getHashUrlParams(),
                        G = [];
                    jQuery.each(z, function (F, h) {
                        D[F] = h
                    });
                    jQuery.each(D, function (F, h) {
                        h != "" && h !== null && G.push(F + "=" + encodeURIComponent(h).replace(/%2C/g, ","))
                    });
                    window.location.hash = l = G.length ? "#" + G.join("&") : ""
                },
                getIdFromClass: function (z, w) {
                    if ((z = w ? q.exec(jQuery(z).attr("class")) : p.exec(jQuery(z).attr("class"))) && z.length > 1) return z[1];
                    return null
                },
                getTypeFromClass: function (z) {
                    if ((z = _reClassType.exec(jQuery(z).attr("class"))) && z.length > 1) return z[1];
                    return null
                },
                highlight: function (z, w, D) {
                    D = D || "em";
                    var G = g(z).toLowerCase().replace(/[<>]+/g, "");
                    w = g(w).toLowerCase().replace(/[<>]+/g, "");
                    G = G.replace(new RegExp(w, "g"), "<" + w + ">");
                    w = 0;
                    for (var F = "", h = 0, L = G.length; h < L; h++) {
                        var W = z.charAt(w),
                            J = G.charAt(h);
                        if (J === "<") F += "<" + D + ">";
                        else if (J === ">") F += "</" + D + ">";
                        else {
                            w++;
                            F += W
                        }
                    }
                    return F
                }
            },
			
			/**
			 *
			 */
			ajaxifyForm: function (selectorScope) {
        if(!selectorScope) {
          return;
        }

        $("form:not(.direct)", $(selectorScope)).live("submit", function(){
          var url = "";
         var form = jQuery(this);
          var type = form.attr("method");

          if(type==undefined)
            type = "get";

          if(type=="get") {

            var action = jQuery(this).attr("action");
            if(action.indexOf("?")==-1)
              url = action + "?" + jQuery(this).serialize();
            else
              url = action + "&"	+ jQuery(this).serialize();

            $.bbq.pushState({ url: url});

          } else {
            jQuery.ajax({
              type:"post",
              dataType:"json",
              data:$(this).serialize(),
              url:jQuery(this).attr("action"),
              success:function(data,textStatus,jqXHR) {

                if(data.status=='error') {
                  var filterId = form.attr('id');
                  var filterContent = $('#'+filterId, $(data.content)).html();
                  form.html(filterContent);
                }
                wkd.applyAjaxFormSubmitted
              }
            });
          }


          return false;
        })
      },

      applyAjaxFormSubmitted: function (obj) {

      },

			applyAjaxPageLoad: function (obj) {

				// First, set contents:
				for(k in obj) {
					if(k.substr(0,1) == "#") {
						//if(k=="#content")
						//	jQuery(k).hide("slow").html(obj[k]).show("slow");
						//else
							jQuery(k).html(obj[k]);
					}
				}
				// Run JS
				if(obj.bootload) {
					wkd.setBootScripts(obj.bootload);
				}
				// Load files
				if(obj.bootload_files) {
					wkd.loadBootScriptFiles(obj.bootload_files);
				}
				if(obj.css_files) {
					wkd.loadCssScripts(obj.css_files);
				}
				// Preserve some links to trigger ajax request
				$(".grid-view table thead tr th a").addClass("direct");
				$(".grid-view table tbody tr td a.delete").addClass("direct");
			},
			
			/**
			 * This is called by CGridView after view has been updated
			 */
			afterGridAjaxUpdate: function (id, data) {
				// Preserve some links to trigger ajax request
				$(".grid-view table thead tr th a").addClass("direct");
				$(".grid-view table tbody tr td a.delete").addClass("direct");
			},
			
			/**
			 * Load wkd Widget component
			 * Loads widget file, adds to core and initializes it
			 * @param string id Script File URL
			 */
			loadWidget: function(id, options) {
				$LAB.script(id).wait(function() {
					jQuery.each(wkd.widgets, function (z, w) {
						if(!d["widgets." + z])
							d["widgets." + z] = w.init(options); // Do not re-initiate widgets
					});
				});
				
			},
			
			/**
			 * 
			 */
			setBootScripts: function(scripts) {
				BootloadScripts = scripts;
			},
			
			/**
			 * 
			 */
			loadBootScriptFiles: function(data) {
				$LAB.script(data).wait(Pub.applyBootscripts);
			},
			applyBootscripts: function() {
				for(k in BootloadScripts) {
					eval(BootloadScripts[k]);
				}
			},
			/**
			 * 
			 */
			loadCssScripts: function(data) {
				var head =  $("head");
				for(k in data) {
					head.append("<link>");
					css = head.children(":last");
					css.attr({
					  rel:  "stylesheet",
					  type: "text/css",
					  href: data[k]
					});
					//console.log('Loaded script ' + data[k]);
				}
			},
			
			/**
			 *
			 */
			onAjaxFormSubmit: function(e) {
				e.preventDefault();
				var formEl = $(this);
				
				$.ajax({
					type: "POST",
					//url: formEl.attr('action') + '?ajax',
					url: formEl.attr('action'),
					data: formEl.serialize(),
					success: function(data, status, moreUselessData) {
						var isJson = data.substr(0, 1) == '{';
						if(isJson) {
							var obj = jQuery.parseJSON(data);
							if(obj && obj.pk) {
								// saved
								frmwrk.success('Success');
								
								// Add Option (@todo call callback
								/* employeePositionDropdown.prepend(
									$("<option></option>").val(obj.pk).html(obj.name)
								); */
								// Select newly created position
								$("#newEntityDialog").dialog("close");
								return true;
							}
						} else {
							var isError = $(data).find("div.error");
							// Check if there were any errors 
							if(isError.length > 0) {
								var output = $(data).find('form#company-form').html();
								$("#newEntityDialog form").html(output);
								//registerPositionBoxCallbacks();
							} else {
								$.pnotify({
									pnotify_title: 'New job position successfully created.',
								});
								$("#newEntityDialog").dialog("close");
							}
						}
					},
				});
			},
			/**
			 *
			 */
            init: function () {
				
				// Get dependant JS
				//$LAB.script('/js/humanmsg.js');
			
				jQuery.each(wkd.widgets, function (z, w) {
                    d["widgets." + z] = w.init()
                });
				frmwrk = Pub.lib;
				/*return;
               
                jQuery.each(wkd.widgets, function (z, w) {
                    d["widgets." + z] = w.init()
                });
                jQuery.each(wkd.views, function (z, w) {
                    d["views." + z] = w.init()
                });
                l = window.location.hash;
                setInterval(function () {
                    if (window.location.hash != l) {
                        l = window.location.hash;
                        jQuery(window.location).trigger("changehash", Pub.lib.getHashUrlParams())
                    }
                }, 400)*/
            }
        };
    return Pub
}(jQuery);

/**
 * Global AJAX error handler
 */
$(document).ajaxError(function(event, request, settings){
	/* $.pnotify({
		pnotify_title: '������!',
		pnotify_text: request.responseText + "\n" + settings.url,
		pnotify_type: 'error'
	}); */
	alert(request.responseText);
});
//jQuery(document).ready(wkd.init);