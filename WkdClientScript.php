<?php
/* 
 * WkdClientScript component
 *
 * @author Sergii 'b3atb0x' <hello@webkadabra.com>
 * @version 1.3
 * @changelog
 *	[+] renderer will apply class to <body> tag
 *	1.3:
	[+] update from chrono
 */
include 'lib/EClientScript.php';

class WebAppAssetsBaseBehavior extends CBehavior
{
	// Web application end's name.
	private $_assetsBaseUrl;

	// Getter.
	// Allows to get the current -end's name
	// this way: Yii::app()->endName;
	public function getAssetsBase($path = null)
	{
		return Yii::app()->clientScript->getProtectedAssetsBase($path);
		#return $this->_assetsBaseUrl;
	}
}

class WkdClientScript extends EClientScript
{
	/**
	 * @param bool Whether or not component should apply additional CSS classes to <body> tag
	 */
	public $applyHelperClasses = true;
	/**
	 * Whether it is allowed to process and cache files on-the-fly
	 */
	public $workOnTheFly = false;
	/**
	 * Global client reset suffix
	 */
	public $resetSuffix;
	/**
	 *
	 */
	public $ajaxifyPageloads = false;
	/**
	 * @var bool should we autoregister base.css (required for some of wkd modules)
	 */
	public $wkdCoreCss = false;
	public $wkdCoreLibrary = false;
	public $labLoader = false;

	public $protectedAssets = false;
	public $protectedAssetsPath = 'www.assets.protected';
	#public $protectedAssetsBypassBaseUrl='/assets/protected';
	public $protectedAssetsBypassBaseUrl = null;
	public $allowBypassOnDebug = true;

	protected $_assetsUrl = null;
	protected $_protectedAssetsBase = null;
	protected $_bootloadScripts = array();
	protected $_bootloadScriptFiles = array();
	protected $_bootloadCssFiles = array();

	/**
	 * Handles publishing of the assets (images etc).
	 * @return string URL of assets folder
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(
				__DIR__ . DIRECTORY_SEPARATOR . 'assets'
			);
		}
		return $this->_assetsUrl;
	}

	public function init()
	{
		if (!$this->resetSuffix)
			$this->resetSuffix = 1;

		Yii::app()->attachBehavior('assetsBaseBehavior', 'WebAppAssetsBaseBehavior');

		Yii::import('wkd.components.clientscript.*');
		Yii::import('wkd.components.clientscript.lib.*');


		parent::init();
	}

	public function getProtectedAssetsBase($path = null)
	{
		if (!$this->protectedAssets) {
			return $path;
		}
		if ($path === null) {
			// useful for developers only:
			if (defined('YII_DEBUG') && YII_DEBUG == true && $this->protectedAssetsBypassBaseUrl) {
				$path = $this->protectedAssetsBypassBaseUrl;
				return $path;
			} else
				$path = $this->protectedAssetsPath;
		}
		if (!isset($this->_protectedAssetsBase[$path])) {
			$this->_protectedAssetsBase[$path] = Yii::app()->assetManager->publish(
				Yii::getPathOfAlias($path),
				false,
				-1,
				false
			#defined('YII_DEBUG') && YII_DEBUG
			);
		}
		return $this->_protectedAssetsBase[$path];
	}

	public function isBootLoadEnabled()
	{
		return (Yii::app()->request->isAjaxRequest && $this->wkdCoreLibrary) == true;
	}

	/**
	 * Registers a CSS file
	 * @param string URL of the CSS file
	 * @param string media that the CSS file should be applied to. If empty, it means all media types.
	 */
	public function _registerCssFile($url, $media = '')
	{
		#if(!$this->compressCss) {
		#	return parent::registerCssFile($url,$media);
		#}

		// Get cached file url
		#$cachedFileUrl = $this->getCachedFilePath($url, $type='css');
		if ($this->isBootLoadEnabled()) {
			$this->_bootloadCssFiles[] = $url;
		} else {
			return parent::registerCssFile($url, $media);
		}
		#parent::registerCssFile($cachedFileUrl,$media);
	}

	public function _registerScript($id, $script, $position = null, array $htmlOptions = array())
	{
		if ($this->isBootLoadEnabled()) {
			$this->_bootloadScripts[$id] = $script;
		} else {
			return parent::registerScript($id, $script, $position);
		}
	}

	public function _registerScriptFile($url, $position = null, array $htmlOptions = array())
	{
		if ($this->isBootLoadEnabled()) {
			$this->_bootloadScriptFiles[] = $url;
		} else {
			return parent::registerScriptFile($url, $position);
		}
	}

	public function getBootloadCssFiles()
	{
		return $this->_bootloadCssFiles;
	}

	public function getBootloadScriptFiles()
	{
		return $this->_bootloadScriptFiles;
	}

	public function getBootloadScripts()
	{
		return $this->_bootloadScripts;
	}

	/**
	 * Boot-load file
	 * @todo Register bootloader JS code itself
	 * @todo ????????????? ??? ?????? wkdLoadwidget() ? ???? onload event ? ???????? ??? ??? ??????????
	 * @param string URL of the file to load
	 */
	public function bootloadScriptFile($url, $options = array())
	{
		/*$this->registerScript(
			base64_encode($url),
			'$LAB.script("'.$url.'");',
			CClientScript::POS_LOAD
		);*/

		// Add Wkd Component
		$this->registerScript(
			$url,
			sprintf('wkd.loadWidget("%s?%d"%s);',
				$url,
				((YII_DEBUG == true) ? mt_rand(1, 999999999) : $this->resetSuffix), // $this->resetSuffix,

				($options ? CJavaScript::encode($options) : null)
			),
			CClientScript::POS_LOAD
		);
	}

	public function registerWkdWidgetFile($url, $options = array())
	{
		return $this->bootloadScriptFile($url, $options);
	}

	/**
	 * Get cached file URL
	 *
	 * @param string $url File url
	 * @param string $type File type (css, js)
	 */
	/* public function getCachedFilePath($url, $type)
	{
		// If we have it cached and stored
		if ($data = ClientScriptCacheRecords::getCachedFilePath($url, $type)) {
			return $data->url;
		} else { // We don't have it cached yet
			// if ($this->workOnTheFly) {
				// return $this->cacheFile($url, $type);
			// }
			return $url;
		}
	} */

	/**
	 * render
	 */
	public function _render(&$output)
	{
		if ($this->wkdCoreLibrary === true) {
			$this->registerScriptFile($this->assetsUrl . '/wkd.core.js', CClientScript::POS_HEAD);
			$this->registerScript('wkd.init', 'wkd.init();', CClientScript::POS_READY);
		}

		if ($this->ajaxifyPageloads) {
			$this->registerCoreScript('jquery');
			$this->registerCoreScript('bbq');
		}

		if ($this->labLoader && !Yii::app()->request->isAjaxRequest)
		{
			// Load all

//			.script("framework.js").wait()
//				.script("plugin.framework.js")
//				.script("myplugin.framework.js").wait()
//				.script("init.js").wait();

			if ($this->scriptFiles) {
				$initScript = array();
				$prev_pos = null;
				foreach ($this->scriptFiles as $pos => $files) {

					if ($files) {
						if ($prev_pos !== null && $prev_pos != $pos) {
							$initScript[] = 'wait()';
						}
						$prev_pos = $pos;
						foreach ($files as $id => $url) {
							$initScript[] = 'script("' . $url . '")';
//							 $this->registerScript(
//								 base64_encode($id),
//								 '$LAB.script("'.$url.'");',
//								 CClientScript::POS_LOAD
//							 );

							$this->scriptMap[$url] = false;
							unset($this->scriptFiles[$pos][$id]);
						}
					}

				}

				// onload
				$onDone = '';
				if (isset($this->scripts[self::POS_READY])) {
					$onDone .= implode("\n", $this->scripts[self::POS_READY]);
					unset($this->scripts[self::POS_READY]);
				}
				if (isset($this->scripts[self::POS_LOAD])) {
					$onDone .= implode("\n", $this->scripts[self::POS_LOAD]);
					unset($this->scripts[self::POS_LOAD]);
				}

				if ($initScript) {
					$this->registerScript(
						'LAB.bootload',
						'$LAB.' . implode('.', $initScript) . '.wait(function(){' . $onDone . '});',
						CClientScript::POS_LOAD
					);

				}
			}

			if ($this->cssFiles) {
				// LAB.js does not serve CSS
			}
		}

		if ($this->wkdCoreCss)
			$this->registerCssFile($this->assetsUrl . '/base.css');

		return parent::render($output);
	}

    /**
     * @param string $output
     */
    public function render(&$output)
    {
        parent::render($output);
        if ($this->combineScriptFiles === true)
            $output = str_replace('<script type="text/javascript" src', '<script async="async" type="text/javascript" src', $output);
    }
    
	/**
	 * WkdRendered main method
	 */
	public function renderBodyBegin(&$output)
	{
		if ($this->ajaxifyPageloads) {
			$this->initiateAjaxifiedPages();
		}

		if ($this->applyHelperClasses === true) {
			$this->applyHelperClasses($output);
		}

		parent::renderBodyBegin($output);
	}

	/**
	 * Add page body's classes
	 */
	private function applyHelperClasses(&$output)
	{
		$classes = array(); // classes to apply to <body> tag

		// user auth indicating class
		if (Yii::app()->user->isGuest)
			$classes[] = 'user-guest';
		else /* if user is logged-in */
		$classes[] = 'user-logged';

		// check and add this only if viewport.js has been added?
		$classes[] = 'no-js';

		$bodyTagHtmlOptions = array();

		// controller-defined body class?
		if (($controller = Yii::app()->getController()) !== null) {
			if (isset($controller->bodyClass) AND $controller->bodyClass) { // check isset since there might be no 'bodyClass' property at controller class at all
				$classes[] = $controller->bodyClass;
			}
			if (isset($controller->bodyHtmlOptions) AND $controller->bodyHtmlOptions) { // check isset since there might be no 'bodyClass' property at controller class at all
				$bodyTagHtmlOptions = $controller->bodyHtmlOptions;
			}

		}

		// Do nothing, if there's no body class to apply to page
		if (!$classes && !$controller->bodyHtmlOptions) {
			return;
		}

		if (isset($bodyTagHtmlOptions['class'])) {
			$classes[] = $bodyTagHtmlOptions['class'];
			unset($bodyTagHtmlOptions['class']);
		}
		// We might only have empty <body> tag in markup, so let's try to avoid RegExp
		if (stristr($output, '<body>')) { // Ok, just replace <body>

			$bodyTagHtmlOptions['class'] = implode(' ', $classes);

			$bodyTag = CHtml::tag('body', $bodyTagHtmlOptions);
			$output = str_replace('<body>', $bodyTag, $output);
		} else {
			// Do the same as above, but respecting (possible) <body> tag attributes as well as inline styles
			// @TODO: Apply body html options!
			if (preg_match('/<body([^>]*)class=["|\']([^>"\']*)["|\']([^>]*)>/is', $output, $match)) {
				if ($match[2]) { // $match[2] contains body markup class
					$classes[] = trim($match[2]);
					$output = preg_replace('/<\bbody([^>]*)class=["|\']([^>"\']*)["|\']([^>]*)>/is', '<body$1<###bodyClass###>$3>', $output, 1, $count);
					if ($count) {
						$output = str_replace('<###bodyClass###>', 'class="' . implode(' ', $classes) . '"', $output);
					}
				}
			}
		}
	}

	/**
	 *
	 */
	private function initiateAjaxifiedPages()
	{
		$this->registerWkdWidgetFile('/assets/wkd/wkd.widget.livePages.js');
	}
}